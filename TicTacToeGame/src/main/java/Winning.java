import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Winning {

    ArrayList<Integer> playerPositions = new ArrayList<Integer>();
    ArrayList<Integer> cpuPositions = new ArrayList<Integer>();

    public String checkWinner() {
        List topRow = Arrays.asList(1, 2, 3);
        List centerRow = Arrays.asList(4, 5, 6);
        List lowRow = Arrays.asList(7, 8, 9);
        List firstC = Arrays.asList(1, 4, 7);
        List secondC = Arrays.asList(2, 5, 8);
        List thirdC = Arrays.asList(3, 6, 9);
        List x1 = Arrays.asList(1, 5, 9);
        List x2 = Arrays.asList(7, 5, 3);

        List<List> winning = new ArrayList<List>();
        winning.add(topRow);
        winning.add(centerRow);
        winning.add(lowRow);
        winning.add(firstC);
        winning.add(secondC);
        winning.add(thirdC);
        winning.add(x1);
        winning.add(x2);

        for (List l : winning) {
            if (playerPositions.containsAll(l)) {
                return "Player wins!";
            } else if (cpuPositions.containsAll(l)) {
                return "CPU wins!";
            } else if (playerPositions.size() + cpuPositions.size() == 9) {
                return "Draw!";
            }

        }
        return " ";
    }
}
