import java.util.Random;
import java.util.Scanner;

public class ChangePosition {
    Integer position;
    Random cpuPosition;
    Winning winner = new Winning();


    public void scanPosition() {
        Scanner enterPosition = new Scanner(System.in);
        System.out.println("Enter the position from 1 to 9!");
        position = enterPosition.nextInt();


    }


    public int getCpuPosition() {
        Random cpuPosition = new Random();
        int cpuIntPosition = cpuPosition.nextInt(9) + 1;

        return cpuIntPosition;
    }

    public int getPosition() {
        return position;
    }

    public void changePositionPlayer(Character[][] board, int position, String user) {
        char symbol = 'x';
        if (user.equals("player")) {
            symbol = 'x';
            winner.playerPositions.add(position);
        } else if (user.equals("cpu")) {
            symbol = 'O';
            winner.cpuPositions.add(position);
        }
        switch (position) {
            case 1:
                board[0][0] = symbol;
                break;
            case 2:
                board[0][2] = symbol;
                break;
            case 3:
                board[0][4] = symbol;
                break;
            case 4:
                board[2][0] = symbol;
                break;
            case 5:
                board[2][2] = symbol;
                break;
            case 6:
                board[2][4] = symbol;
                break;
            case 7:
                board[4][0] = symbol;
                break;
            case 8:
                board[4][2] = symbol;
                break;
            case 9:
                board[4][4] = symbol;
                break;
            default:
                break;

        }

    }
}
