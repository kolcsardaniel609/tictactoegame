public class GameBoard {

    private Character[][] gameBoard = new Character[][]{{' ', '|', ' ', '|', ' '},
            {'-', '+', '-', '+', '-'},
            {' ', '|', ' ', '|', ' '},
            {'-', '+', '-', '+', '-'},
            {' ', '|', ' ', '|', ' '}};


    public void printBoardGame() {

        for (Character[] row : gameBoard) {
            for (char column : row) {
                System.out.print(column);
            }
            System.out.println();
        }
    }

    public Character[][] getGameBoard() {
        return gameBoard;
    }
}
