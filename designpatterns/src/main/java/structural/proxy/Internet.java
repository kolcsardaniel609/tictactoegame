package structural.proxy;

public class Internet implements InternetInterface{

    @Override
    public void connectTo(String server) throws Exception{
        System.out.println("Connecting to " + server);
    }
}
