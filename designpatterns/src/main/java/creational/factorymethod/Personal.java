package creational.factorymethod;

public interface Personal {

    public enum PersonalType {
        STUDENT,
        TEACHER
    }

    public void work();

}
